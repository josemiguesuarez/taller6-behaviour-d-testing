Feature: Opening the help screen

  Scenario: As a user I want to be able to calculate a path for my travel
    Given I press "Menú de navegación cerrado"
    And I press "Asignar alarma para transbordos"
    And I press "Planear viaje"
    And I press "Transmilenio"
    And I press "Punto de origen"
    And I press "Punto de destino"
    And I press "Cambiar punto de origen con punto de destino"
    And I press "Troncales"
    And I press "Taxis Libres"    
    When  I press "Punto de origen"
    And I enter text "Pol" into field with id "etSearch"
    And I wait for "Polo" to appear
    And I press "Polo"
    And I wait for "Punto de destino" to appear
    And I press "Punto de destino"
    And I enter text "El Tiempo" into field with id "etSearch"
    And I press "El Tiempo / Maloka"
    And I press view with id "btnOrigenDestino"
    And I press "Cambiar"
    And I press "spnDia"
    And I press "Lunes"
    And I press "AM"
    And I press "Switch to text input mode for the time input."
    And I clear input field with id "input_hour"
    And I enter text "10" into field with id "input_hour"
    And I go back
    And I press view with id "button1"
    
    
    Then I should see "Inicio en Polo "
    And I should see "Hasta Av. Chile"