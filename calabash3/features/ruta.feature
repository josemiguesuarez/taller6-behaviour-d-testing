Feature: Find a transmilenio service

  Scenario: As a user I want to find a transmilenio service
    Given I press "Rutas de buses"
    And I press "Rutas de buses"
    And I press "Search"
    And I enter text "J24" into field with id "search_src_text"
    And I press the enter button 
    Then I should see "Portal 80 » Universidades"