**Taller 6**

Cucumber 

Se creó un nuevo ejemplo para agregar el caso de login exitoso 
[ver código](https://gitlab.com/josemiguesuarez/taller6-behaviour-d-testing/blob/master/cucumber-webdriverio/features/login.feature)
y se agregó el código para soportarlo [ver código](https://gitlab.com/josemiguesuarez/taller6-behaviour-d-testing/blob/master/cucumber-webdriverio/features/step-definitions/index.js)

Se creó un feature para probar el registro en la página. Se realizó un total de 5 escenarios que prueban que el formulario esté correcto y que no haya un usuario registrado con el mismo correo 
[ver código](https://gitlab.com/josemiguesuarez/taller6-behaviour-d-testing/blob/master/cucumber-webdriverio/features/signup.feature).
Adicionalmente se agregó el código para soportar las pruebas basadas en el comportamiento  [ver código](https://gitlab.com/josemiguesuarez/taller6-behaviour-d-testing/blob/master/cucumber-webdriverio/features/step-definitions/index.js)
