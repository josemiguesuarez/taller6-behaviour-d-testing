Feature: Sign up into losestudiantes
    As an user I want to create an account

Scenario Outline: Sign up fails

  Given I go to losestudiantes home screen
    When I open the login screen
    And I fill the form with <name>, <lastname>, <email>, <password>, <university> and <deparment>
    And I <accept> terms and conditions
    And I try to sign up
    Then I expect to see <error> in <place>

Examples:
| name | lastname | email | password | university | deparment | accept | error | place |
| Prueba | Prueba | cupitallerensayo@gmail.com | 1234 | Universidad Nacional | Derecho | accept | La contraseña debe ser al menos de 8 caracteres | form |
| Prueba | Prueba | cupitallerensayo@ñp | 12345678 | Universidad de los Andes | Derecho | accept | Ingresa un correo valido | form |
| Prueba | Prueba | cupitallerensayo@gmail.com |  | Universidad de la Sabana | Enfermería | accept | Ingresa una contraseña | form |
| Prueba | Prueba | cupitallerensayo@gmail.com | 12345678 | Escuela Colombiana de Ingeniería | Economía | not | Debes aceptar los términos y condiciones | form |
| Prueba | Prueba | cupitallerensayo@gmail.com | 12345678 | Universidad Icesi | Diseño Industrial | accept | Ocurrió un error activando tu cuenta | pop up |