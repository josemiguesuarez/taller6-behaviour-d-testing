var {
    defineSupportCode
} = require('cucumber');
var {
    expect
} = require('chai');

defineSupportCode(({
    Given,
    When,
    Then
}) => {
    Given('I go to losestudiantes home screen', () => {
        browser.url('/');
        if (browser.isVisible('button=Cerrar')) {
            browser.click('button=Cerrar');
        }
    });

    When('I open the login screen', () => {
        browser.waitForVisible('button=Ingresar', 5000);
        browser.click('button=Ingresar');
    });

    When('I fill a wrong email and password', () => {
        var cajaLogIn = browser.element('.cajaLogIn');

        var mailInput = cajaLogIn.element('input[name="correo"]');
        mailInput.click();
        mailInput.keys('wrongemail@example.com');

        var passwordInput = cajaLogIn.element('input[name="password"]');
        passwordInput.click();
        passwordInput.keys('123467891');
    });

    When('I try to login', () => {
        var cajaLogIn = browser.element('.cajaLogIn');
        cajaLogIn.element('button=Ingresar').click();
    });

    Then('I expect to not be able to login', () => {
        browser.waitForVisible('.aviso.alert.alert-danger', 5000);
    });

    When(/^I fill with (.*) and (.*)$/, (email, password) => {
        var cajaLogIn = browser.element('.cajaLogIn');

        var mailInput = cajaLogIn.element('input[name="correo"]');
        mailInput.click();
        mailInput.keys(email);

        var passwordInput = cajaLogIn.element('input[name="password"]');
        passwordInput.click();
        passwordInput.keys(password);
    });

    Then(/^I expect to (not)? see (.*)$/, (not, str) => {
        console.log("see, str", not, str);
        browser.pause(1000);
        if (not) {
            var buttons = browser.getHTML('button');
            buttons.forEach(function (button) {
                console.log("button::::", button);
                expect(button).to.not.include(str);
            });
        } else {
            browser.waitForVisible('.aviso.alert.alert-danger', 5000);
            var alertText = browser.element('.aviso.alert.alert-danger').getText();
            expect(alertText).to.include(str);
        }
    });

    When(/^I fill the form with (.*), (.*), (.*), (.*), (.*) and (.*)$/, (name, lastname, email, password, university, deparment) => {
        console.log("Searching for caja sign up");
        var cajaSignUp = browser.element('.cajaSignUp');
        console.log(" caja sign up", cajaSignUp);

        write(cajaSignUp.element('input[name="nombre"]'), name);
        write(cajaSignUp.element('input[name="apellido"]'), lastname);
        write(cajaSignUp.element('input[name="correo"]'), email);
        write(cajaSignUp.element('input[name="password"]'), password);
        cajaSignUp.element('select[name="idUniversidad"]').selectByVisibleText(university);
        browser.pause(500);
        cajaSignUp.element('select[name="idDepartamento"]').selectByVisibleText(deparment);
    });
    When(/^I (accept|not) terms and conditions$/, (accept) => {
        if (accept === "accept") {
            var cajaSignUp = browser.element('.cajaSignUp');
            cajaSignUp.element('input[name="acepta"]').click();
        }
    });

    When('I try to sign up', () => {
        var cajaSignUp = browser.element('.cajaSignUp');
        cajaSignUp.element('button=Registrarse').click();
    });

    Then(/^I expect to see (.*) in (.*)$/, (str, place) => {
        console.log("string::: ", str, place);
        if (place === 'form') {
            browser.waitForVisible('.aviso.alert.alert-danger', 5000);
            let elements = browser.elements('.aviso.alert.alert-danger').value;
            let errorMessages = elements.map(element => browser.elementIdText(element.ELEMENT).value);
            expect(errorMessages).to.include(str);
        } else {
            browser.waitForVisible('.sweet-alert', 5000);
            var alertText = browser.element('.sweet-alert').element('h2').getText();
            expect(alertText).to.have.string(str);
        }

    });





});

function write(element, text) {
    element.click();
    element.keys(text);
}